package pl.pmajewski.xtm.recruitment.config;

public class Constants {

    public static final String WIKI_BASE = "https://en.wikipedia.org";
    public static final String WIKI_BASE_SEARCH = WIKI_BASE + "/w/api.php";
    public static final String WIKI_DEFINITION_BASE = WIKI_BASE + "/wiki/";
}
