package pl.pmajewski.xtm.recruitment.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.pmajewski.xtm.recruitment.config.Constants;
import pl.pmajewski.xtm.recruitment.dto.WikiResponse;
import pl.pmajewski.xtm.recruitment.dto.WikiResponseRecord;
import pl.pmajewski.xtm.recruitment.repository.WikiRepository;

@RestController
public class WikiSearch {

    private WikiRepository repository;

    public WikiSearch(WikiRepository repository) {
        this.repository = repository;
    }

    @GetMapping("wiki/search/{value}")
    public String search(@PathVariable String value) {
        WikiResponse wikiResponse = repository.search(value);
        return repository.search(value)
                .getQuery()
                .getSearch()
                .stream()
                .filter(i -> i.getTitle().toUpperCase().contains(value.toUpperCase()))
                .filter(i -> i.getSnippet().toUpperCase().contains("football club".toUpperCase()))
                .map(WikiResponseRecord::getTitle)
                .map(title -> title.replace(" ", "_"))
                .map(i -> Constants.WIKI_DEFINITION_BASE+i)
                .findFirst().orElseGet(() -> "");
    }
}
